type nat =
  | O
  | S of nat

type comparable =
  | Int of Z.t
  | Nat of nat
  | String of (Pervasives.char) seq
  | Bytes of t seq
  | Mutez of Z.t
  | Bool of bool
  | Key_hash of (Pervasives.char) seq
  | Timestamp of (Pervasives.char) seq

let compare_int (a: Z.t) (b: Z.t) : Z.t = Z.sub a b

let compare_bool (a: bool) (b: bool) : Z.t =
  begin match (a, b) with
  | (false, true) -> Z.of_string "-1"
  | (true, false) -> Z.one
  | (_, _) -> Z.zero
  end

let compare_string (a: (Pervasives.char) seq) (b: (Pervasives.char) seq) :
  Z.t =
  let rec aux (a1: (Pervasives.char) seq) (b1: (Pervasives.char) seq)
              (i: Z.t) : Z.t =
    if (Z.lt i (length a1)) && (Z.lt i (length b1)) then begin
      let (x, y) =
      (Z.of_int (Char.code (mixfix_lbrb a1 i)),
      Z.of_int (Char.code (mixfix_lbrb b1 i))) in
      if Z.equal x y then begin aux a1 b1 (Z.add i Z.one) end
      else
      begin
        Z.sub x y end end
    else
    begin
      Z.zero end in aux a b Z.zero

let eval_nat (natValue: nat) : Z.t =
  let rec aux1 (acc: Z.t) (natValue1: nat) : Z.t =
    begin match natValue1 with
    | O -> acc
    | S n -> aux1 (Z.add acc Z.one) n
    end in aux1 Z.zero natValue

let compare_comparable (a: comparable) (b: comparable) : Z.t =
  begin match (a, b) with
  | (Int x1, Int y1) -> compare_int x1 y1
  | (Nat x1, Nat y1) -> compare_int (eval_nat x1) (eval_nat y1)
  | (Bool x1, Bool y1) -> compare_bool x1 y1
  | (String x1, String y1) | (Key_hash x1, Key_hash y1) | (Timestamp x1,
    Timestamp y1) -> compare_string x1 y1
  | _ -> assert false (* absurd *)
  end

type comparable_t =
  | Int_t
  | Nat_t
  | String_t
  | Bytes_t
  | Mutez_t
  | Bool_t
  | Key_hash_t
  | Timestamp_t

type typ =
  | Comparable_t of comparable
  | Key_t
  | Unit_t
  | Signature_t
  | Option_t of typ
  | List_t of typ
  | Set_t of comparable
  | Operation_t
  | Address_t
  | Contract_t of typ
  | Pair_t of typ * typ
  | Or_t of typ * typ
  | Lambda_t of typ * typ
  | Map_t of comparable * typ
  | Big_map_t of comparable * typ

type data =
  | Instruction of instruction
  | Comparable of comparable
  | Key of data
  | Unit
  | Some of data
  | None
  | List of data list
  | Pair of data * data
  | Left of data
  | Right of data
  | Set of comparable -> (bool)
  | Map of comparable -> data
  | Big_map of comparable -> data
and instruction =
  | Seq_i of instruction * instruction
  | NOOP
  | DROP
  | DUP
  | SWAP
  | PUSH of typ * data
  | SOME
  | NONE of typ
  | UNIT
  | IF_NONE of instruction * instruction
  | PAIR
  | CAR
  | CDR
  | LEFT of typ
  | RIGHT of typ
  | IF_LEFT of instruction * instruction
  | IF_RIGHT of instruction * instruction
  | NIL of typ
  | CONS
  | IF_CONS of instruction * instruction
  | SIZE
  | EMPTY_SET of comparable
  | EMPTY_MAP of comparable * typ
  | MAP of instruction
  | ITER of instruction
  | MEM
  | GET
  | UPDATE
  | IF of instruction * instruction
  | LOOP of instruction
  | LOOP_LEFT of instruction
  | LAMBDA of typ * typ * instruction
  | EXEC
  | DIP of instruction
  | FAILWITH of data
  | CAST
  | RENAME
  | CONCAT
  | SLICE
  | PACK
  | UNPACK
  | ADD
  | SUB
  | MUL
  | EDIV
  | ABS
  | NEG
  | LSL
  | LSR
  | OR
  | AND
  | XOR
  | NOT
  | COMPARE
  | EQ
  | NEQ
  | LT
  | GT
  | LE
  | GE
  | SELF
  | CONTRACT of typ
  | TRANSFER_TOKENS
  | SET_DELEGATE
  | CREATE_ACCOUNT
  | CREATE_CONTRACT
  | CREATE_CONTRACT_I of instruction
  | IMPLICIT_ACCOUNT
  | NOW
  | AMOUNT
  | BALANCE
  | CHECK_SIGNATURE
  | BLAKE2B
  | SHA256
  | SHA512
  | HASH_KEY
  | STEPS_TO_QUOTA
  | SOURCE
  | SENDER
  | ADDRESS

type stack_t = data list

exception Out_of_fuel

exception Abort of (data list)

let eval_data : type xi . xi ->  data = fun _ ->  Unit

let infix_clcl : type a . a -> (a list) ->  (a list) = fun a b ->  a :: b

let peek : type xi1 . (xi1 list) ->  xi1 =
  fun s ->  begin match s with
    | h :: _ -> h
    | [] -> raise (Abort [])
    end

let pop : type xi2 . (xi2 list) ->  (xi2 * (xi2 list)) =
  fun s ->  begin match s with
    | h :: t1 -> (h, t1)
    | [] -> raise (Abort [])
    end

let to_nat (n: Z.t) : nat =
  let rec aux2 (i: Z.t) (acc: nat) : nat =
    if Z.geq i n then begin acc end
    else
    begin
      aux2 (Z.add i Z.one) (S acc) end in aux2 Z.zero O

let rec add_nat (n: nat) (m: nat) : nat =
  begin match m with
  | O -> n
  | S mqt -> add_nat (S n) mqt
  end

let rec mult_nat (n: nat) (m: nat) : nat =
  begin match n with
  | O -> O
  | S p -> add_nat m (mult_nat p m)
  end

let is_zero_nat (n: nat) : bool =
  begin match n with
  | O -> true
  | _ -> false
  end

let fst : type xi3 xi4 . (xi3 * xi4) ->  xi3 = fun z ->  let (a, _) = z in a

let rec divmod_nat (x1: nat) (y1: nat) (q: nat) (u: nat) : nat * nat =
  begin match x1 with
  | O -> (q, u)
  | S xqt ->
    begin match u with
    | O -> divmod_nat xqt y1 (S q) y1
    | S uqt -> divmod_nat xqt y1 q uqt
    end
  end

let div_nat (x1: nat) (y1: nat) : nat =
  begin match y1 with
  | O -> y1
  | S yqt -> fst (divmod_nat x1 yqt O yqt)
  end

let rec sub_nat (n: nat) (m: nat) : nat =
  begin match (n, m) with
  | (S k, S l) -> sub_nat k l
  | (_, _) -> n
  end

let snd : type xi5 xi6 . (xi5 * xi6) ->  xi6 = fun z ->  let (_, b) = z in b

let modulo_nat (x1: nat) (y1: nat) : nat =
  begin match y1 with
  | O -> y1
  | S yqt -> sub_nat yqt (snd (divmod_nat x1 yqt O yqt))
  end

let rec eval (inStack: data list) (fuel: Z.t) (instr: instruction) :
  data list =
  if Z.equal fuel Z.zero then begin raise Out_of_fuel end
  else
  begin
    begin match instr with
    | FAILWITH _ -> raise (Abort inStack)
    | NOOP -> inStack
    | Seq_i (a1, b1) ->
      let o = eval inStack (Z.sub fuel Z.one) a1 in
      eval o (Z.sub fuel Z.one) b1
    | IF_NONE (bt, bf) ->
      begin match let o = peek inStack in eval_data o with
      | Comparable Bool b1 ->
        if b1 then begin eval inStack (Z.sub fuel Z.one) bt end
        else
        begin
          eval inStack fuel bf end
      | _ -> raise (Abort inStack)
      end
    | LOOP body as s ->
      begin match let o = peek inStack in eval_data o with
      | Comparable Bool b1 ->
        if b1 then begin
          eval inStack (Z.sub fuel Z.one) (Seq_i ((body, s))) end
        else
        begin
          inStack end
      | _ -> raise (Abort inStack)
      end
    | LOOP_LEFT body as s1 ->
      begin match let o = peek inStack in eval_data o with
      | Left a1 ->
        eval (infix_clcl a1 inStack) (Z.sub fuel Z.one) (Seq_i ((body, s1)))
      | Right a1 -> infix_clcl a1 inStack
      | _ -> raise (Abort inStack)
      end
    | DIP code ->
      let o = eval inStack (Z.sub fuel Z.one) code in
      let o1 = peek inStack in infix_clcl o1 o
    | EXEC ->
      let (a1, t1) = pop inStack in let (f, t2) = pop t1 in
      begin match eval_data f with
      | Instruction i ->
        let s2 = eval (infix_clcl a1 []) (Z.sub fuel Z.one) i in
        begin match s2 with
        | r :: [] -> infix_clcl r t2
        | _ -> raise (Abort inStack)
        end
      | _ -> raise (Abort inStack)
      end
    | DROP -> let (_, t3) = pop inStack in t3
    | DUP -> let h = peek inStack in infix_clcl h inStack
    | SWAP ->
      let (h1, t11) = pop inStack in let (h2, t21) = pop t11 in
      infix_clcl h2 (infix_clcl h1 t21)
    | PUSH (_, a2) -> infix_clcl a2 inStack
    | UNIT -> infix_clcl Unit inStack
    | LAMBDA (_, _, someCode) -> infix_clcl (Instruction someCode) inStack
    | EQ ->
      let (h, t4) = pop inStack in
      let o =
        begin match eval_data h with
        | Comparable Int i ->
          Comparable (Bool ((Z.equal (compare_int i Z.zero) Z.zero) || false))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t4
    | NEQ ->
      let (h3, t5) = pop inStack in
      let o =
        begin match eval_data h3 with
        | Comparable Int i ->
          Comparable (Bool ((not (Z.equal (compare_int i Z.zero) Z.zero)) || false))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t5
    | LT ->
      let (h4, t6) = pop inStack in
      let o =
        begin match eval_data h4 with
        | Comparable Int i ->
          Comparable (Bool ((Z.lt (compare_int i Z.zero) Z.zero) || false))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t6
    | GT ->
      let (h5, t7) = pop inStack in
      let o =
        begin match eval_data h5 with
        | Comparable Int i ->
          Comparable (Bool ((Z.gt (compare_int i Z.zero) Z.zero) || false))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t7
    | LE ->
      let (h6, t8) = pop inStack in
      let o =
        begin match eval_data h6 with
        | Comparable Int i ->
          Comparable (Bool ((Z.leq (compare_int i Z.zero) Z.zero) || false))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t8
    | GE ->
      let (h7, t9) = pop inStack in
      let o =
        begin match eval_data h7 with
        | Comparable Int i ->
          Comparable (Bool ((Z.geq (compare_int i Z.zero) Z.zero) || false))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t9
    | OR ->
      let (x1, t12) = pop inStack in let (y1, t22) = pop t12 in
      let o =
        begin match (eval_data x1, eval_data y1) with
        | (Comparable Bool x2, Comparable Bool y2) ->
          Comparable (Bool (x2 || y2))
        | (Comparable Nat x2, Comparable Nat y2) ->
          let res =
            to_nat (to_int (bw_or (of_int (eval_nat x2))
                              (of_int (eval_nat y2)))) in
          Comparable (Nat res)
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t22
    | AND ->
      let (x2, t13) = pop inStack in let (y2, t23) = pop t13 in
      let o =
        begin match (eval_data x2, eval_data y2) with
        | (Comparable Bool x3, Comparable Bool y3) ->
          Comparable (Bool (x3 && y3))
        | (Comparable Nat x3, Comparable Nat y3) ->
          let res =
            to_nat (to_int (bw_and (of_int (eval_nat x3))
                              (of_int (eval_nat y3)))) in
          Comparable (Nat res)
        | (Comparable Int x3, Comparable Nat y3) ->
          let res =
            to_nat (to_int (bw_and (of_int x3) (of_int (eval_nat y3)))) in
          Comparable (Nat res)
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t23
    | XOR ->
      let (x3, t14) = pop inStack in let (y3, t24) = pop t14 in
      let o =
        begin match (eval_data x3, eval_data y3) with
        | (Comparable Bool x4, Comparable Bool y4) ->
          Comparable (Bool ((x4 || y4) && (not (x4 && y4))))
        | (Comparable Nat x4, Comparable Nat y4) ->
          let res =
            to_nat (to_int (bw_xor (of_int (eval_nat x4))
                              (of_int (eval_nat y4)))) in
          Comparable (Nat res)
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t24
    | NOT ->
      let (x4, t10) = pop inStack in
      let o =
        begin match eval_data x4 with
        | Comparable Bool x5 -> Comparable (Bool (not x5))
        | Comparable Nat x5 ->
          Comparable (Int (to_int (bw_not (of_int (eval_nat x5)))))
        | Comparable Int x5 -> Comparable (Int (to_int (bw_not (of_int x5))))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t10
    | NEG ->
      let (x5, t15) = pop inStack in
      let o =
        begin match eval_data x5 with
        | Comparable Nat x6 -> Comparable (Int (Z.neg (eval_nat x6)))
        | Comparable Int x6 -> Comparable (Int (Z.neg x6))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t15
    | ABS ->
      let (x6, t16) = pop inStack in
      let o =
        begin match eval_data x6 with
        | Comparable Int x7 -> Comparable (Nat (to_nat (Z.abs x7)))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t16
    | ADD ->
      let (x7, t17) = pop inStack in let (y4, t25) = pop t17 in
      let o =
        begin match (eval_data x7, eval_data y4) with
        | (Comparable Int x8, Comparable Int y5) ->
          Comparable (Int (Z.add x8 y5))
        | (Comparable Int x8, Comparable Nat y5) ->
          Comparable (Int (Z.add x8 (eval_nat y5)))
        | (Comparable Nat x8, Comparable Int y5) ->
          Comparable (Int (Z.add (eval_nat x8) y5))
        | (Comparable Nat x8, Comparable Nat y5) ->
          Comparable (Nat (add_nat x8 y5))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t25
    | SUB ->
      let (x8, t18) = pop inStack in let (y5, t26) = pop t18 in
      let o =
        begin match (eval_data x8, eval_data y5) with
        | (Comparable Int x9, Comparable Int y6) ->
          Comparable (Int (Z.sub x9 y6))
        | (Comparable Int x9, Comparable Nat y6) ->
          Comparable (Int (Z.sub x9 (eval_nat y6)))
        | (Comparable Nat x9, Comparable Int y6) ->
          Comparable (Int (Z.sub (eval_nat x9) y6))
        | (Comparable Nat x9, Comparable Nat y6) ->
          Comparable (Int (Z.sub (eval_nat x9) (eval_nat y6)))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t26
    | MUL ->
      let (x9, t19) = pop inStack in let (y6, t27) = pop t19 in
      let o =
        begin match (eval_data x9, eval_data y6) with
        | (Comparable Int x10, Comparable Int y7) ->
          Comparable (Int (Z.mul x10 y7))
        | (Comparable Int x10, Comparable Nat y7) ->
          Comparable (Int (Z.mul x10 (eval_nat y7)))
        | (Comparable Nat x10, Comparable Int y7) ->
          Comparable (Int (Z.mul (eval_nat x10) y7))
        | (Comparable Nat x10, Comparable Nat y7) ->
          Comparable (Nat (mult_nat x10 y7))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t27
    | EDIV ->
      let (x10, t110) = pop inStack in let (y7, t28) = pop t110 in
      let o =
        begin match (eval_data x10, eval_data y7) with
        | (Comparable Int x11, Comparable Int y8) ->
          if Z.equal y8 Z.zero then begin None end
          else
          begin
            let (res, rem) = (Z.ediv x11 y8, Z.erem x11 y8) in
            Some (Pair ((Comparable (Int res), Comparable (Nat (to_nat rem))))) end
        | (Comparable Int x11, Comparable Nat y8) ->
          if is_zero_nat y8 then begin None end
          else
          begin
            let (res1, rem1) =
            (Z.ediv x11 (eval_nat y8), Z.erem x11 (eval_nat y8)) in
            Some (Pair ((Comparable (Int res1),
                        Comparable (Nat (to_nat rem1))))) end
        | (Comparable Nat x11, Comparable Int y8) ->
          if Z.equal y8 Z.zero then begin None end
          else
          begin
            let (res2, rem2) =
            (Z.ediv (eval_nat x11) y8, Z.erem (eval_nat x11) y8) in
            Some (Pair ((Comparable (Int res2),
                        Comparable (Nat (to_nat rem2))))) end
        | (Comparable Nat x11, Comparable Nat y8) ->
          if is_zero_nat y8 then begin None end
          else
          begin
            let (res3, rem3) = (div_nat x11 y8, modulo_nat x11 y8) in
            Some (Pair ((Comparable (Nat res3), Comparable (Nat rem3)))) end
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t28
    | LSL ->
      let (x11, t111) = pop inStack in let (y8, t29) = pop t111 in
      let o =
        begin match (eval_data x11, eval_data y8) with
        | (Comparable Nat x12, Comparable Nat y9) ->
          if Z.leq (eval_nat y9) (Z.of_string "256") then begin
            let (xqt, yqt) = (of_int (eval_nat x12), eval_nat y9) in
            Comparable (Nat (to_nat (to_int (lsl1 xqt yqt)))) end
          else
          begin
            raise (Abort inStack) end
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t29
    | LSR ->
      let (x12, t112) = pop inStack in let (y9, t210) = pop t112 in
      let o =
        begin match (eval_data x12, eval_data y9) with
        | (Comparable Nat x13, Comparable Nat y10) ->
          let (xqt1, yqt1) = (of_int (eval_nat x13), eval_nat y10) in
          Comparable (Nat (to_nat (to_int (lsr1 xqt1 yqt1))))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t210
    | COMPARE ->
      let (x13, t113) = pop inStack in let (y10, t211) = pop t113 in
      let o =
        begin match (eval_data x13, eval_data y10) with
        | (Comparable Int x14, Comparable Int y11) ->
          Comparable (Int (compare_int x14 y11))
        | (Comparable Nat x14, Comparable Nat y11) ->
          let (xqt2, yqt2) = (eval_nat x14, eval_nat y11) in
          Comparable (Int (compare_int xqt2 yqt2))
        | (Comparable String x14, Comparable String y11) ->
          let tmp = compare_string x14 y11 in
          let res4 =
            if Z.lt tmp Z.zero then begin Z.of_string "-1" end
            else
            begin
              if Z.gt tmp Z.zero then begin Z.one end else begin Z.zero end end in
          Comparable (Int res4)
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t211
    | CONCAT ->
      let (x14, t114) = pop inStack in let (y11, t212) = pop t114 in
      let o =
        begin match (eval_data x14, eval_data y11) with
        | (Comparable String x15, Comparable String y12) ->
          Comparable (String (infix_plpl x15 y12))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t212
    | SIZE ->
      let (x15, t115) = pop inStack in
      let o =
        begin match eval_data x15 with
        | Comparable String x16 -> Comparable (Nat (to_nat (length x16)))
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t115
    | PAIR ->
      let (x16, t116) = pop inStack in let (y12, t213) = pop t116 in
      infix_clcl (Pair ((x16, y12))) t213
    | CAR ->
      let (x17, t117) = pop inStack in
      let o =
        begin match eval_data x17 with
        | Pair (a2, _) -> a2
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t117
    | CDR ->
      let (x18, t118) = pop inStack in
      let o =
        begin match eval_data x18 with
        | Pair (_, b1) -> b1
        | _ -> raise (Abort inStack)
        end in
      infix_clcl o t118
    | _ -> raise (Abort inStack)
    end end

let pred_nat (n: nat) : nat = begin match n with
  | O -> O
  | S p -> p
  end

